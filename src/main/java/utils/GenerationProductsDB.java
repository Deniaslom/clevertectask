package utils;

import beans.Product;
import factory.EnumDAOFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerationProductsDB {
    private static Random ran = new Random();
    private static String[] productsMassiv = {"PRODUCT_WITHOUT_DIS", "PRODUCT_DISCOUNT"};
    public static List<Product> products = getProducts(100);

    private static volatile GenerationProductsDB instance;

    private GenerationProductsDB() {
    }

    public static GenerationProductsDB getInstance(){
        if(instance == null)
            synchronized (GenerationProductsDB.class){
                if(instance == null)
                    instance = new GenerationProductsDB();
            }
        return instance;
    }


    public static List<Product> getProducts(int quantity) {
        List<Product> products = new ArrayList<>();

        for (int i = 0; i < quantity; i++) {
            Product product = EnumDAOFactory.valueOf(productsMassiv[ran.nextInt(2)]).getDAO();

            product.setId(i);
            product.setName("Product_" + i);
            product.setCount(randomCount());
            product.setPrice(randomPrice());

            products.add(product);
        }

        return products;
    }

    public static List<Product> getProducts() {
        return products;
    }

    private static double randomPrice() {
        return ((double) (ran.nextInt(150) + 40)) / 100;
    }

    private static int randomCount() {
        return (ran.nextInt(4) + 3);
    }
}
