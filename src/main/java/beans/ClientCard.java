package beans;

import java.util.Objects;

public class ClientCard {
    private int id;
    private int discount;

    public ClientCard() {
    }

    public ClientCard(int id, int discount) {
        this.id = id;
        this.discount = discount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientCard that = (ClientCard) o;
        return id == that.id &&
                discount == that.discount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, discount);
    }

    @Override
    public String toString() {
        return id + ";" + discount + "; ";
    }


}
