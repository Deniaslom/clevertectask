package factory;

import beans.ProductWithDiscount;
import beans.ProductWithoutDiscount;
import beans.Product;

/**
 * this class implements the factory pattern.
 */
public enum EnumDAOFactory {
    PRODUCT_DISCOUNT {
        @Override
        public Product getDAO() {
            Product product = new ProductWithDiscount();
            return product;
        }
    },

    PRODUCT_WITHOUT_DIS {
        @Override
        public Product getDAO() {
            Product product = new ProductWithoutDiscount();
            return product;
        }
    };

    public abstract Product getDAO();


}
